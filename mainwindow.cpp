#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qdebug.h"
#include <iostream>
#include <cstdlib>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

// задать параметры кривой
void MainWindow::on_Elliptic_curve_param_accept_clicked()
{
    if((ui->Elliptic_curve_param_p->toPlainText().replace(" ","")=="")||(ui->Elliptic_curve_param_a->toPlainText().replace(" ","")=="")||(ui->Elliptic_curve_param_b->toPlainText().replace(" ","")=="")){ // проверка условий для инициализации
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Задайте параметры эллептической кривой.")); // вывод ошибки
    }else{
        // записисываем параметры p,a и b, что бы проверить на сингулярность
        char *buff_p=ui->Elliptic_curve_param_p->toPlainText().replace(" ","").toLatin1().data();
        bignum_fromhex(EC.p, buff_p, ECCRYPT_BIGNUM_DIGITS);
        QString p = QString(buff_p);
        delete buff_p;
        char *buff_a=ui->Elliptic_curve_param_a->toPlainText().replace(" ","").toLatin1().data();
        bignum_fromhex(EC.a, buff_a, ECCRYPT_BIGNUM_DIGITS);
        QString a = QString(buff_a);
        delete buff_a;
        char *buff_b=ui->Elliptic_curve_param_b->toPlainText().replace(" ","").toLatin1().data();
        bignum_fromhex(EC.b, buff_b, ECCRYPT_BIGNUM_DIGITS);
        QString b = QString(buff_b);
        delete buff_b;

        if(eccrypt_is_sing(EC)){
            QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Заданная элептическая кривая сингулярна, введите другие параметры.")); // вывод ошибки
        }
        else{

            if(ui->Elliptic_curve_param_m->toPlainText().replace(" ","")==""){ // проверка условий для инициализации
                QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Задайте порядок группы точек эллептической кривой.")); // вывод ошибки
            }else{
                // если кривая не сингулярна, то идем дальше
                // проверяем порядок группы точек кривой и порядок подгруппы
                char *buff_m=ui->Elliptic_curve_param_m->toPlainText().replace(" ","").toLatin1().data();
                bignum_fromhex(EC.m, buff_m, ECCRYPT_BIGNUM_DIGITS);
                QString m = QString(buff_m);
                delete buff_m;
                if(bignum_cmp(EC.p,EC.m, ECCRYPT_BIGNUM_DIGITS)==0){
                    QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Порядок группы точек эллептической кривой m равен параметру эллептической кривой p. Поменяйте один из параметров.")); // вывод ошибки
                }else{
                    if(ui->Elliptic_curve_param_q->toPlainText().replace(" ","")==""){ // проверка условий для инициализации
                        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Задайте порядок циклической подгруппы точек эллептической кривой.")); // вывод ошибки
                    }else{
                        char *buff_q=ui->Elliptic_curve_param_q->toPlainText().replace(" ","").toLatin1().data();
                        bignum_fromhex(EC.q, buff_q, ECCRYPT_BIGNUM_DIGITS);
                        QString q = QString(buff_q);
                        delete buff_q;
                        bignum_div(EC.m,EC.q,NULL,EC.g.y,ECCRYPT_BIGNUM_DIGITS); // тут координата Y образующей точки прямой выступает пока в роли
                        //буффера для хранения результата деления m на q
                        if(!bignum_iszero(EC.g.y,ECCRYPT_BIGNUM_DIGITS)){
                            QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Параметр q не делит нацело параметр m. Измените параметры.")); // вывод ошибки
                        }else{
                            if((ui->P_x->toPlainText().replace(" ","")=="")||(ui->P_y->toPlainText().replace(" ","")=="")){ // проверка условий для инициализации
                                QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Задайте координаты генерирующей точки.")); // вывод ошибки
                            }else{
                                // если дошли до сюда, то все условия для выбора кривой верны, а значит можно принять параметры
                                ui->Elliptic_curve_state->setText("Элептическая кривая: \na="+a+"\nb="+b+"\np="+p+"\nнесингулярна");
                                ui->Elliptic_curve_param_m_state->setText("Порядок группы точек элептической кривой m: "+m);
                                ui->Elliptic_curve_param_q_state->setText("Порядок циклической подгруппы точек элептической кривой q: "+q);
                                char *buff_Px=ui->P_x->toPlainText().replace(" ","").toLatin1().data();
                                bignum_fromhex(EC.g.x, buff_Px, ECCRYPT_BIGNUM_DIGITS);
                                QString Px = QString(buff_Px);
                                delete buff_Px;
                                char *buff_Py=ui->P_y->toPlainText().replace(" ","").toLatin1().data();
                                bignum_fromhex(EC.g.y, buff_Py, ECCRYPT_BIGNUM_DIGITS);
                                QString Py = QString(buff_Py);
                                delete buff_Py;
                                ui->P_state->setText("P("+Px+", "+Py+")");
                                EC.g.is_inf = false;
                            }
                        }
                    }
                }
            }
        }
    }
}

// кадать ключ подписания
void MainWindow::on_Signature_key_accept_clicked()
{
    if((ui->Elliptic_curve_state->text()=="Элептическая кривая: не задана")){ // проверка условий
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Задайте параметры эллептической кривой.")); // вывод ошибки
    }else{
        if((ui->Signature_key->toPlainText().replace(" ","")=="")){ // проверка условий
            QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Задайте ключ подписи d.")); // вывод ошибки
        }else{
            // записываем ключ подписания
            char *buff_signature_key=ui->Signature_key->toPlainText().replace(" ","").toLatin1().data();
            bignum_fromhex(Signature_key, buff_signature_key, ECCRYPT_BIGNUM_DIGITS);
            QString str_signature_key = QString(buff_signature_key);
            delete buff_signature_key;
            ui->Signature_key_state->setText("Ключ подписи d: "+str_signature_key);
            // вычисляем ключ проверки подписи и выводим его в виджет
            Q = eccrypt_point_mul(EC.g, Signature_key, EC);
            char *buff_Qx=new char[BIGNUM_DIGITS(*Q.x)];
            bignum_tohex(Q.x, buff_Qx, BIGNUM_DIGITS(*Q.x), ECCRYPT_BIGNUM_DIGITS);
            ui->Signature_verification_key_state->setText("Ключ проверки подписи Q: ("+QString(buff_Qx)+",\n");
            delete[] buff_Qx;
            char *buff_Qy=new char[BIGNUM_DIGITS(*Q.y)];
            bignum_tohex(Q.y, buff_Qy, BIGNUM_DIGITS(*Q.y), ECCRYPT_BIGNUM_DIGITS);
            ui->Signature_verification_key_state->setText(ui->Signature_verification_key_state->text()+QString(buff_Qy)+")");
            delete[] buff_Qy;
        }
    }
}

// открить файл
void MainWindow::on_Open_file_clicked()
{
    QString address = QFileDialog::getOpenFileName(0,tr("Open File"),"C:/Users/Sunny3drag0n/Documents/build-podpis-Desktop_Qt_5_9_1_MinGW_32bit-Debug/"); // вызываем диалоговое окно, а полоченный от него адрес запоминаем
    if (address!=""){ // если пользователь не отменил открытие файла
        ui->Msg_state->setText("Последний открытый файл: "+address); // выводим путь файла в виджет
        QFile file(address); // открываем файл
        if (!file.open(QIODevice::ReadOnly)) // если не можем из него читать
        {
            QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Ошибка открытия для чтения.")); // вывод ошибки
        }else{// а если можем
            QByteArray ba;
            ba = file.readAll(); // считываем в байтовый массив
            QString text = ba; // байтовым массивом заполняем строку
            text =  text.toLocal8Bit().toHex(); // строку конвертируем в hex
            // можем оставить так, но для наглядности распарсим из вида ?????... к виду ?? ?? ?...
            char *result= new char[text.length()]; // буфер только в char
            result=text.toLatin1().data();

            QString outtext=""; // строка в которой будет результат
            QString strout=""; // строка в которой будет пара символов ??
            char buffer [3]; // буфер
            for(int i = 0 ; i < text.length(); i++){
                strout="";

                itoa (result[i],buffer,16);
                strout.append(buffer);

                while(strout.length()<2){
                    strout.prepend("0");
                }
                strout.append(" ");
                outtext = outtext + strout;
            }
            ui->Msg->setText(outtext);
            delete [] result;
        }
        file.close(); // закрываем файл
    }
}

// вычислить подпись
void MainWindow::on_Calculate_EDS_clicked()
{

    unsigned short error = 0;
    //проверка на достаточность данных для вычислений
    if((ui->Signature_key_state->text()=="Ключ подписи d: не задан")){ // проверка условий
        error = 2;
    }
    if((ui->Elliptic_curve_state->text()=="Элептическая кривая: не задана")){ // проверка условий
        error = 1;
    }

    switch(error){

    case 1:{
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Задайте параметры эллептической кривой.")); // вывод ошибки
        break;

    }
    case 2:{
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Задайте ключ подписи d.")); // вывод ошибки
        break;
    }

    default:{
        // вычисление хеша сообщения
        QStringList strmsg = ui->Msg->toPlainText().split(" ", QString::SkipEmptyParts); // смотрим, что ввел пользовател, или что получилось после открытия файла

        if(strmsg.count()==0){ // если вообще ничего не было введено примем за 0x00
            strmsg.append("00");
        }
        uint8_t *data = new uint8_t[strmsg.length()]; // переводим все в массив, что бы подать на вход стрибогу
        for(int i = 0 ; i < _msize(data)/sizeof(data[0]) ; i++)
        {
            QByteArray ba = strmsg.at(i).toLatin1();
            const char *c_str = ba.data();
            data[i] = strtol(c_str,NULL,16);
        }

        vector hash; // вектор в ктором будет результат
        unsigned int length_hash=0; // сколько в нем байт, понадобится для вывода результата в виджет
        if(ECCRYPT_BIGNUM_DIGITS * sizeof(EC.q[0])*8==512){ // если у нас q представляется 512 битами
            memcpy(hash,stribog_512(data),BLOCK_SIZE); // используем стрибог для 512 бит
            length_hash=BLOCK_SIZE; // а длина хеша весь блок
        }
        if(ECCRYPT_BIGNUM_DIGITS * sizeof(EC.q[0])*8==256){ // если 256 битами
            memcpy(hash,stribog_256(data),BLOCK_SIZE); // то стрибог соответственно для 256 бит
            length_hash=BLOCK_SIZE/2; // а длина хеша пол блока
        }
        ui->Last_hash->setText(""); // чистим виджет с предыдущим результатом хеширования
        QString strout;
        char buffer [3];
        for(int i = 0 ; i < length_hash ; i++){ // пишем в него новый результат
            strout="";

            itoa (hash[i],buffer,16);
            strout.append(buffer);

            while(strout.length()<2){
                strout.prepend("0");
            }
            strout.append(" ");
            ui->Last_hash->setText(ui->Last_hash->toPlainText() + strout);
        }

        // находим альфа
        char *last_hash=ui->Last_hash->toPlainText().replace(" ","").toLatin1().data(); // представляем полученный хеш как строку
        bignum_fromhex(alpha, last_hash, ECCRYPT_BIGNUM_DIGITS); // строку переводим в большое число
        delete last_hash;

        // вычисляем e
        bignum_div(alpha, EC.q, 0, e, ECCRYPT_BIGNUM_DIGITS); // берем альфа по модулю q
        if(bignum_iszero(e, ECCRYPT_BIGNUM_DIGITS)){ // если получился ноль
            e[0] = 1; // принимаем за еденицу
        }

        // если необходиио проверить контрольное значение из примера ГОСТа, то раскоментируйте строку ниже
        //bignum_fromhex(e, "2DFBC1B372D89A1188C09C52E0EEC61FCE52032AB1022E8E67ECE6672B043EE5", ECCRYPT_BIGNUM_DIGITS);

        bignum_setzero(r, ECCRYPT_BIGNUM_DIGITS); // обнуляем вектора, для последующей удобной работы
        bignum_setzero(s, ECCRYPT_BIGNUM_DIGITS);

        // формируем подпись
        while(bignum_iszero(s, ECCRYPT_BIGNUM_DIGITS)){ // что бы вектор s не был нулевым
            while(bignum_iszero(r, ECCRYPT_BIGNUM_DIGITS)){ // что бы вектор r не был нулевым

                // генерируем k
                for(unsigned int i = 0 ; i < ECCRYPT_BIGNUM_DIGITS ; i++){ // заполняем случайными значениями
                    k[i]=rand();
                }
                bignum_div(k, EC.q, 0, k, ECCRYPT_BIGNUM_DIGITS); // берем его по модулю для того что бы 0<k<q
                // существует вероятность что rand() сгенерирует k=0, но выполнять лишнюю проверку - тратить ресурсы

                // если необходиио проверить контрольное значение из примера ГОСТа, то раскоментируйте строку ниже
                //bignum_fromhex(k, "77105C9B20BCD3122823C8CF6FCC7B956DE33814E95B7FE64FED924594DCEAB3", ECCRYPT_BIGNUM_DIGITS);

                C = eccrypt_point_mul(EC.g, k, EC);// вычисляем точку C=kP

                bignum_div(C.x, EC.q, 0, r, ECCRYPT_BIGNUM_DIGITS); // берем его по модулю для того что бы 0<k<q
            }
            //вычмсляем s=(rd+ke) mod q
            bignum_digit_t second_term[ECCRYPT_BIGNUM_DIGITS];
            bignum_cpy(second_term, k, ECCRYPT_BIGNUM_DIGITS, ECCRYPT_BIGNUM_DIGITS);
            bignum_mmul(second_term, e, EC.q, ECCRYPT_BIGNUM_DIGITS);
            bignum_cpy(s, r, ECCRYPT_BIGNUM_DIGITS, ECCRYPT_BIGNUM_DIGITS);
            bignum_mmul(s, Signature_key, EC.q, ECCRYPT_BIGNUM_DIGITS);
            bignum_madd(s, second_term, EC.q, ECCRYPT_BIGNUM_DIGITS);
        }
        // пишем полученные значения в виджеты
        char *buff_r=new char[BIGNUM_DIGITS(*r)];
        bignum_tohex(r, buff_r, BIGNUM_DIGITS(*r), ECCRYPT_BIGNUM_DIGITS);
        ui->vector_r->setText(buff_r);
        delete[] buff_r;

        char *buff_s=new char[BIGNUM_DIGITS(*s)];
        bignum_tohex(s, buff_s, BIGNUM_DIGITS(*s), ECCRYPT_BIGNUM_DIGITS);
        qDebug()<<hex<<buff_s;
        ui->vector_s->setText(buff_s);
        delete[] buff_s;
        break;
    }

    }
}

// проверить подпись
void MainWindow::on_Check_EDS_clicked()
{
    unsigned short error = 0;

    if((ui->vector_s->toPlainText().replace(" ","")=="")){ // проверка s не пуст
        error = 4;
    }
    if((ui->vector_r->toPlainText().replace(" ","")=="")){ // проверка r не пуст
        error = 3;
    }
    if((ui->Signature_verification_key_state->text()=="Ключ проверки подписи Q: не возможно рассчитать")){ // проверка условий
        error = 2;
    }
    if((ui->Elliptic_curve_state->text()=="Элептическая кривая: не задана")){ // проверка условий
        error = 1;
    }

    switch(error){
    case 1:{
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Задайте параметры эллептической кривой.")); // вывод ошибки
        break;
    }
    case 2:{
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Задайте ключ проверки подписи Q.")); // вывод ошибки
        break;
    }
    case 3:{
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Вектор r подписи ζ не задан.")); // вывод ошибки
        break;
    }
    case 4:{
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Вектор s подписи ζ не задан.")); // вывод ошибки
        break;
    }

    default:{
        // проверяем 0<r<q и 0<s<q
        // так как мы работаем с беззнаковыми числами вместо r>0 и s>0 достаточно проверить r!=0 и s!=0
        char *buff_r=ui->vector_r->toPlainText().toLatin1().data();
        bignum_fromhex(r, buff_r, ECCRYPT_BIGNUM_DIGITS);
        qDebug()<< "вектор r"<<buff_r;
        delete[] buff_r;
        if(bignum_iszero(r, ECCRYPT_BIGNUM_DIGITS)||(bignum_cmp(r, EC.q, ECCRYPT_BIGNUM_DIGITS)!=-1)){
            ui->Last_EDS_state->setText("Последняя проверенная подпись: неверна\nПроваленное условие: 0<r<q");
            break;
        }
        char *buff_s=ui->vector_s->toPlainText().toLatin1().data();
        bignum_fromhex(s, buff_s, ECCRYPT_BIGNUM_DIGITS);
        delete[] buff_s;
        if(bignum_iszero(s, ECCRYPT_BIGNUM_DIGITS)||(bignum_cmp(s, EC.q, ECCRYPT_BIGNUM_DIGITS)!=-1)){
            ui->Last_EDS_state->setText("Последняя проверенная подпись: неверна\nПроваленное условие: 0<s<q");
            break;
        }

        // вычисление хеша сообщения
        QStringList strmsg = ui->Msg->toPlainText().split(" ", QString::SkipEmptyParts);

        if(strmsg.count()==0){ // если вообще ничего не было введено примем за 0x00
            strmsg.append("00");
        }
        uint8_t *data = new uint8_t[strmsg.length()];
        for(int i = 0 ; i < _msize(data)/sizeof(data[0]) ; i++)
        {
            QByteArray ba = strmsg.at(i).toLatin1();
            const char *c_str = ba.data();
            data[i] = strtol(c_str,NULL,16);
            //qDebug()<<i<<hex<<data[i];
        }
        vector hash;
        unsigned int length_res=0;
        if(ECCRYPT_BIGNUM_DIGITS * sizeof(EC.q[0])*8==512){
            memcpy(hash,stribog_512(data),BLOCK_SIZE);
            length_res=BLOCK_SIZE;
        }
        if(ECCRYPT_BIGNUM_DIGITS * sizeof(EC.q[0])*8==256){
            memcpy(hash,stribog_256(data),BLOCK_SIZE);
            length_res=BLOCK_SIZE/2;
        }
        ui->Last_hash->setText("");
        QString strout;
        char buffer [3];
        for(int i = 0 ; i < length_res ; i++){
            strout="";

            itoa (hash[i],buffer,16);
            strout.append(buffer);

            while(strout.length()<2){
                strout.prepend("0");
            }
            strout.append(" ");
            ui->Last_hash->setText(ui->Last_hash->toPlainText() + strout);
        }

        // находим альфа
        char *last_hash=ui->Last_hash->toPlainText().replace(" ","").toLatin1().data();
        bignum_fromhex(alpha, last_hash, ECCRYPT_BIGNUM_DIGITS);
        delete [] last_hash;

        // вычисляем e
        bignum_div(alpha, EC.q, 0, e, ECCRYPT_BIGNUM_DIGITS);
        if(bignum_iszero(e, ECCRYPT_BIGNUM_DIGITS)){
            e[0] = 1;
        }

        // если необходиио проверить контрольное значение из примера ГОСТа, то раскоментируйте строку ниже
        //bignum_fromhex(e, "2DFBC1B372D89A1188C09C52E0EEC61FCE52032AB1022E8E67ECE6672B043EE5", ECCRYPT_BIGNUM_DIGITS);

        // вычисляем v
        bignum_cpy(v, e, ECCRYPT_BIGNUM_DIGITS, ECCRYPT_BIGNUM_DIGITS);
        bignum_inv(v, EC.q, ECCRYPT_BIGNUM_DIGITS);

        // вычисляем z1
        bignum_cpy(z1, s, ECCRYPT_BIGNUM_DIGITS, ECCRYPT_BIGNUM_DIGITS); // сейчас z1=s mod q
        bignum_mmul(z1, v, EC.q, ECCRYPT_BIGNUM_DIGITS); // сейчас z1=sv mod q

        // вычисляем z2
        bignum_cpy(z2, r, ECCRYPT_BIGNUM_DIGITS, ECCRYPT_BIGNUM_DIGITS); // сейчас z2=r mod q
        bignum_digit_t const_zero[ECCRYPT_BIGNUM_DIGITS];
        bignum_setzero(const_zero, ECCRYPT_BIGNUM_DIGITS);
        bignum_msub(const_zero, z2, EC.q, ECCRYPT_BIGNUM_DIGITS); // сейчас const_zero=-r mod q
        bignum_cpy(z2, const_zero, ECCRYPT_BIGNUM_DIGITS, ECCRYPT_BIGNUM_DIGITS); // сейчас z2=r mod q
        bignum_mmul(z2, v, EC.q, ECCRYPT_BIGNUM_DIGITS); // сейчас z2=-rv mod q

        // вычисляем C
        C = eccrypt_point_sum(eccrypt_point_mul(EC.g, z1, EC), eccrypt_point_mul(Q, z2, EC), EC);

        // вычисляем R
        bignum_cpy(R, C.x, ECCRYPT_BIGNUM_DIGITS, ECCRYPT_BIGNUM_DIGITS);
        bignum_div(R, EC.q, 0 , R, ECCRYPT_BIGNUM_DIGITS);

        // проверяем R=r
        if(bignum_cmp(R, r, ECCRYPT_BIGNUM_DIGITS)!=0){
            ui->Last_EDS_state->setText("Последняя проверенная подпись: неверна\nПроваленное условие: R=r");
            break;

        }
        ui->Last_EDS_state->setText("Последняя проверенная подпись: верна");
        break;
    }
    }
}
